@extends('layouts.app')

@section('content')
                <div class="card-header">Create Berita</div>

                <div class="card-body">
                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                        @endforeach
                    @endif
                    {!! Form::model($berita , ['url'=>route('berita.update', $berita->id), 'files'=>'true','method'=>'patch']) !!}
                    
                    <div class="form-group">
                        {!! Form::label('judul', 'Judul Berita', []) !!}
                        {!! Form::text('judul', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('kategori', 'Kategori Berita', []) !!}
                        {!! Form::select('kategori', [
                            'Olahraga' => 'Olahraga',
                            'Politik' => 'Politik',
                            'Kesehatan' => 'Kesehatan',
                        ] , null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('gambar', 'Gambar Berita', []) !!}
                        {!! Form::file('gambar', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('konten', 'Isi Berita', []) !!}
                        {!! Form::textarea('konten', null, ['class'=>'form-control']) !!}
                    </div>

                    {!! Form::submit('Simpan', ['class'=>'btn btn-danger ']) !!}

                    
                    {!! Form::close() !!}
                    
                </div>
@endsection
