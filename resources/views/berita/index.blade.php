@extends('layouts.app')

@section('content')

                <div class="card-header">Berita <span class="float-right"> <a class="btn btn-outline-primary btn-sm" href="{{route('berita.create')}}"> Add </a> </span> </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>    
                                <th>Judul</th>    
                                <th>kategori</th>    
                                <th width="175">Aksi</th>    
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($beritas as $berita )
                            <tr>
                                <td>{{$berita->id}}</td>    
                                <td>{{$berita->judul}}</td>    
                                <td>{{$berita->kategori}}</td>    
                                <td>
                                    <a class="btn btn-outline-success btn-sm" href="{{route('berita.show', $berita->id)}}"> Show </a>
                                    <a class="btn btn-outline-warning btn-sm" href="{{route('berita.edit', $berita->id)}}"> Edit </a>
                                            
                                    {!! Form::open(['url'=>route('berita.destroy', $berita->id), 'class'=>'float-right', 'method'=>'delete']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger btn-sm']) !!}                        
                                    {!! Form::close() !!}
                                </td>    
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4">Data masih kosong</td>        
                            </tr>
                            @endforelse

                        </tbody>
                    </table>
                    {{$beritas->links()}}
                </div>
@endsection

@push('script')
<script>
$('.btn-outline-danger').on('click',function(e){
e.preventDefault();
var form = $(this).parents('form');
swal({
  title: "Anda yakin?",
  text: "Once deleted, you will not be able to recover this data!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    swal("Poof! Your data has been deleted!", {
      icon: "success",
    });
    form.submit();
  } 
});
});
</script>
@endpush