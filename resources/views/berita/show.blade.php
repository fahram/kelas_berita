@extends('layouts.app')

@section('content')

                <div class="card-body">
                        <li class="media">
                            <a class="d-flex" href="#">
                                <img src="{{Storage::url($berita->gambar)}}" alt="">
                            </a>
                            <div class="media-body">
                                <h5>{{$berita->judul}}</h5>
                                <h6>{{$berita->kategori}}</h6>
                                {{$berita->konten}}
                            </div>
                        </li>
                    
                </div>
@endsection
