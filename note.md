### Instalasi 
```bash
composer create-project laravel/laravel berita
```

### database 
```bash
mysql -uroot -e "create database berita"
```

### edit .env
```
DB_DATABASE=berita
DB_USERNAME=root
DB_PASSWORD=
```

### buat scafold auth
```bash
php artisan make:auth
```

### edit migration user
```php
$table->string('role')->default('author');
```
### migrate
```bash
php artisan migrate 
```

### membuat middleware

```bash
php artisan make:middleware HasRole
```
* edit app\Http\Kernel.php
```php
    protected $routeMiddleware = [
        'role' => \App\Http\Middleware\HasRole::class,
    ...
    ];
```
* edit \App\Http\Middleware\Http\HasRole
  
```php


    public function handle($request, Closure $next, $role)
    {
        if($request->user()->role == $role){
            return $next($request);
        }
        return redirect()->route('home')->withErrors(['auth'=>'Anda tidak memiliki akses']);
    }
```

* edit route (web.php)

```php 
```


### install laravelcollective
```bash
composer require "laravelcollective/html"
```


### membuat crud berita

* membuat model, controller & migration
```bash
php artisan make:model -a Berita
```
* membuat route (edit routes/web.php)

```php
Route::resource('berita', 'BeritaController')->middleware('role:admin');
```

* edit file migration (berita.php)
```php

    public function up()
    {
        Schema::create('beritas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->string('kategori');
            $table->text('konten');
            $table->string('gambar')->default('post.png');
            $table->integer('user_id');
            $table->timestamps();
        });
    }
```

### migrate table berita
```bash
php artisan migrate 
```
