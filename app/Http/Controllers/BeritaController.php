<?php

namespace App\Http\Controllers;

use App\Berita;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beritas = Berita::orderBy('id', 'desc')->paginate(10);
        return view('berita.index', compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('berita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:200',
            'konten'    => 'required|min:500',
            'kategori'  => 'required'
        ]);
        $request['user_id'] = Auth()->user()->id;
        $berita = Berita::create($request->except('gambar'));
        if($request->hasFile('gambar')){
           $berita->gambar =  $request->gambar->store('public');
           $berita->save();
        }
        return redirect()->route('berita.index')->with('status','Tambah Data berhasil');
        //dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::find($id);
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::find($id);
        return view('berita.edit', compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:200',
            'konten'    => 'required|min:500',
            'kategori'  => 'required'
        ]);
        $request['user_id'] = Auth()->user()->id;
        $berita = Berita::find($id);
        $berita->update($request->except('gambar'));
        if($request->hasFile('gambar')){
           $berita->gambar =  $request->gambar->store('public');
           $berita->save();
        }
        return redirect()->route('berita.index')->with('status','Ubah Data berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Berita::find($id)->delete();
        return redirect()->route('berita.index')->with('status','Hapus Data berhasil');

    }
}
