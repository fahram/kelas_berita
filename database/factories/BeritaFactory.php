<?php

use Faker\Generator as Faker;

$factory->define(App\Berita::class, function (Faker $faker) {
    return [
        'judul' => $faker->sentence(20),
        'kategori' => $faker->sentence(5),
        'konten' => $faker->text(),
        'gambar' => $faker->imageUrl($width = 640, $height = 480),
        'user_id' => 1,
    ];
});
